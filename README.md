# UnityAI

## Installation

Anaconda, Python 3.7 and Unity 2019.10 or later needed.

This project is based on Unity's ML-Agents Toolkit https://github.com/Unity-Technologies/ml-agents.

### ML-Agents installation

* Download Unity ML-Agents Toolkit https://github.com/Unity-Technologies/ml-agents/archive/release_3.zip.
* Extract it at the root of this project.
* Rename the folder to "ml-agents".
* Open Anaconda's prompt and go inside the "ml-agents" folder.
* Execute ``conda create -n mlagents python=3.7``.
* ``conda activate mlagents``.
* ``cd ml-agents-envs``.
* ``pip install -e .``.
* ``cd ../ml-agents``.
* ``pip install -e .``.

### Training

* Open Anaconda prompt.
* Navigate to the project's directory then the ml-agents folder.
* Type ``conda activate mlagents``.
* Change the training hyperparameters by modifying the config/ppo/UnitBehavior.yaml file.
* Open the project with the Unity editor.
* Type ``mlagents-learn ../config/ppo/UnitBehavior.yaml --run-id=TribeTraining`` in the Anaconda prompt.
* Start the simulation in the Unity editor.

If the training program is not started when the simulation is played then it will need a model to work properly.