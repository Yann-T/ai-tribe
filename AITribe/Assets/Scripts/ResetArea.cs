﻿using System.Collections.Generic;
using UnityEngine;

public class ResetArea : MonoBehaviour
{
    public List<ResetRemoveChild> resets;

    public void Reset()
    {
        foreach (var reset in resets)
        {
            reset.Reset();
        }
    }
}
