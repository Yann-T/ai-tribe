﻿using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// This classe contains functions wo used a NavMesh
/// </summary>
public static class NavMeshUtil
{

    /// <summary>
    /// Get a random point on the NavMesh surface
    /// </summary>
    /// <param name="center">The center of the NavMesh</param>
    /// <param name="maxDistance">The max distance from the center at which we want the point to be chosen</param>
    /// <returns>A point on a NavMesh</returns>
    public static Vector3 GetRandomPoint(Vector3 center, float maxDistance)
    {
        // Get Random Point inside Sphere which position is center, radius is maxDistance
        Vector3 randomPos = Random.insideUnitSphere * maxDistance + center;

        NavMeshHit hit; // NavMesh Sampling Info Container

        // from randomPos find a nearest point on NavMesh surface in range of maxDistance
        NavMesh.SamplePosition(randomPos, out hit, maxDistance, UnityEngine.AI.NavMesh.AllAreas);

        return hit.position;
    }
}
