﻿using UnityEngine;

/// <summary>
/// A controller used for the tribe agent
/// </summary>
public class UnitController : BasicUnitController
{
    #region Properties
    private Rigidbody _rb;
    private TribeAgent _agent;

    public CraftingAbility CraftingAbility { get; set; }
    #endregion

    #region Public methods
    /// <summary>
    /// Processes the action given to the agent
    /// </summary>
    /// <param name="act">The action received from the neural network</param>
    public void ProcessAction(float act)
    {
        var dirToGo = Vector3.zero;

        int action = Mathf.FloorToInt(act);

        switch (action)
        {
            // Don't move
            case 0:
                dirToGo = Vector3.zero;
                break;
            // Move forward
            case 1:
                dirToGo = transform.forward * 1f; _agent.UnitMoved();
                break;
            // Move backward
            case 2:
                dirToGo = transform.forward * -1f; _agent.UnitMoved();
                break;
            // Move left
            case 3:
                dirToGo = transform.right * -1f; _agent.UnitMoved();
                break;
            // Move right
            case 4:
                dirToGo = transform.right * 1f; _agent.UnitMoved();
                break;
            // Craft campfire
            case 5:
                TryToCraft(Structures.Campfire);
                break;
            // Craft wall
            case 6:
                TryToCraft(Structures.Wall);
                break;
            // Gather wood
            case 7:
                TryToCollectWood();
                break;
        }
        _rb.AddForce(dirToGo * _agent.TribeSimulationSettings.agentSpeed * CraftingAbility.SpeedMultiplier(), ForceMode.VelocityChange);
    }

    /// <summary>
    /// Test if the unit can craft and if that is the case start the craft of the given structure
    /// </summary>
    /// <param name="structure">The structure to craft</param>
    private void TryToCraft(Structures structure)
    {
        if (HasCraftingAbility())
        {
            if (!CraftingAbility.StartCrafting(structure))
            {
                _agent.UnitDidWrongAction();  // Add a penality when the agent tries to craft but can't.
            }
            else
            {
                _agent.UnitDidGoodAction();   // Reward the unit when sucessfully crafting something.
            }
        }
    }

    /// <summary>
    /// Test if the unit can craft and if that is the case then start to collect wood if there is a tree nearby
    /// </summary>
    private void TryToCollectWood()
    {
        if (HasCraftingAbility())
        {
            if (!CraftingAbility.StartCollectingWood())
            {
                _agent.UnitDidWrongAction();  // Add a penality when the agent tries to gather wood but can't.
            }
            else
            {
                _agent.UnitDidGoodAction();   // Reward the unit when sucessfully gathering wood.
            }
        }
    }

    /// <summary>
    /// Test if the unit can craft objects
    /// </summary>
    /// <returns>True if the agent can craft false otherwise</returns>
    public bool HasCraftingAbility()
    {
        return CraftingAbility != null;
    }

    /// <summary>
    /// Deal direct damage to the agent
    /// </summary>
    /// <param name="amount">The amount of damage to deal</param>
    public override void DealDamage(float amount, DamageType damageType)
    {
        CurrentHP -= amount;
        if (IsDead())
        {
            ProcessDeath(damageType);
        }
        else
        {
            DamageColorChange();
            _agent.UnitTookDamage(CurrentHP, MaxHP);
        }
    }

    #endregion

    public override void Start()
    {
        base.Start();
        CraftingAbility = GetComponent<CraftingAbility>();
        _agent = GetComponent<TribeAgent>();
        _rb = GetComponent<Rigidbody>();
    }

    #region Death and damage related enum and methods

    /// <summary>
    /// Displays the death message of the agent and disable the agent for the rest of the episode
    /// </summary>
    /// <param name="causeOfDeath">What caused the death of the agent</param>
    protected override void ProcessDeath(DamageType causeOfDeath)
    {
        _agent.UnitDied();
        base.ProcessDeath(causeOfDeath);
    }
    #endregion

    /// <summary>
    /// Resets the unit
    /// </summary>
    public override void Reset()
    {
        base.Reset();
        var temp = GetComponent<TemperatureDamage>();
        CraftingAbility.Reset();
        if (temp != null)
        {
            temp.Reset();
        }
    }
}
