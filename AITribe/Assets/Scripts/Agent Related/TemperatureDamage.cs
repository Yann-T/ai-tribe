﻿using UnityEngine;

/// <summary>
/// This components allows an agent ti take damage from the temperature
/// </summary>
public class TemperatureDamage : MonoBehaviour
{
    #region Properties
    [Tooltip("The speed at which the temperature of an agent varies")]
    public float bodyTemperatureChangeRate = 10f;

    [Tooltip("The amount of damage caused to an agent having a low body temperature")]
    public float lowTemperatureDamageAmount = 1f;
    [Tooltip("The amount of damage caused to an agent having a high body temperature")]
    public float highTemperatureDamageAmount = 1f;

    private AreaTemperature _areaTemperature;
    private float _timeSinceLastDamage;
    private UnitController _unitController;
    [SerializeField]
    private float _currentBodyTemperature;
    public float CurrentBodyTemperature { get => _currentBodyTemperature; set => _currentBodyTemperature = value; }
    #endregion

    private void Start()
    {
        _timeSinceLastDamage = 0f;
        _areaTemperature = GetComponentInParent<AreaTemperature>();
        _unitController = GetComponent<UnitController>();
        CurrentBodyTemperature = 20f;
    }

    void Update()
    {
        float temperatureAtPosition = _areaTemperature.GetTemperatureAtPosition(transform.position);
        if (temperatureAtPosition != CurrentBodyTemperature)
        {
            CurrentBodyTemperature += ((temperatureAtPosition - CurrentBodyTemperature) / bodyTemperatureChangeRate) * Time.deltaTime;
        }
        if (CanTakeTemperatureDamage() && !_unitController.IsDead())
        {
            if (HasLowBodyTemperature())
            {
                _unitController.DealDamage(lowTemperatureDamageAmount, DamageType.Temperature);
                _timeSinceLastDamage = 0;
            }
            else if (HasHighBodyTemperature())
            {
                _unitController.DealDamage(highTemperatureDamageAmount, DamageType.Temperature);
                _timeSinceLastDamage = 0;
            }
        }
        else
        {
            _timeSinceLastDamage += Time.deltaTime;
        }
    }

    /// <summary>
    /// Returns true if the agent has a low enough body temperature to take damage
    /// </summary>
    /// <returns>Whether the agent can take damage from low body temperature</returns>
    private bool HasLowBodyTemperature()
    {
        return CurrentBodyTemperature < _areaTemperature.lowTemperatureDamageThreshold;
    }

    /// <summary>
    /// Returns true if the agent has a high enough body temperature to take damage
    /// </summary>
    /// <returns>Whether the agent can take damage from high body temperature</returns>
    private bool HasHighBodyTemperature()
    {
        return CurrentBodyTemperature > _areaTemperature.highTemperatureDamageThreshold;
    }

    /// <summary>
    /// Returns whether the agent can take damage from the temperature
    /// </summary>
    /// <returns></returns>
    private bool CanTakeTemperatureDamage()
    {
        return _timeSinceLastDamage >= _areaTemperature.timeBetweenTemperatureDamage;
    }

    /// <summary>
    /// Resets the component for the next episode
    /// </summary>
    public void Reset()
    {
        CurrentBodyTemperature = 20f;
        _timeSinceLastDamage = 0f;
    }

}
