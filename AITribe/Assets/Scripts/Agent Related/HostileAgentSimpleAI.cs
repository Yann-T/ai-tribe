﻿using UnityEngine;

/// <summary>
/// A simple AI for an hostile agent who will only try to attack the nearest visible agent.
/// </summary>
public class HostileAgentSimpleAI : MonoBehaviour
{
    #region Properties
    [Header("Related components")]
    public FieldOfView FOV;
    public HostileAgentController Controller;

    [Header("Parameters")]
    [Tooltip("The minimum time (in seconds) between two attack")]
    [Range(0.1f, 3f)]
    public float MinTimeBetweenTwoAttacks = 1f;
    [Tooltip("The distance at which the hostile agent can attack")]
    [Range(0.5f, 3f)]
    public float AttackDistance = 0.5f;
    [Tooltip("Damage dealt per attack")]
    [Range(0.5f, 10f)]
    public float AttackDamage = 2f;

    private float _timeSinceLastAttack = float.MaxValue;
    #endregion

    void Update()
    {
        _timeSinceLastAttack += Time.deltaTime;
        var target = GetClosestTarget();
        if (target != null)
        {
            Controller.MoveToPosition(target.transform.position);
            TryToAttackTarget(target);
        }
    }

    /// <summary>
    /// Instantly change the position of the agent
    /// </summary>
    /// <param name="newPos">The new position</param>
    public void ChangePosition(Vector3 newPos)
    {
        transform.position = newPos;
    }

    /// <summary>
    /// Try to attack the given target
    /// </summary>
    /// <param name="target">The target to attack</param>
    private void TryToAttackTarget(GameObject target)
    {
        if (Vector3.Distance(transform.position, target.transform.position) <= AttackDistance)
        {
            if (_timeSinceLastAttack >= MinTimeBetweenTwoAttacks)
            {
                target.GetComponent<UnitController>().DealDamage(AttackDamage, DamageType.Direct);
                _timeSinceLastAttack = 0f;
            }
        }
    }

    /// <summary>
    /// Returns the closest valid target
    /// </summary>
    /// <returns>The closest valid target or null if none are within range</returns>
    private GameObject GetClosestTarget()
    {
        GameObject closestTarget = null;
        float closestTargetDistance = float.MaxValue;

        foreach (var gameObject in FOV.GetVisibleObjects())
        {
            var controller = gameObject.GetComponent<UnitController>();

            if (controller != null)  // If the gameObject is an agent
            {
                if (!controller.IsDead())
                {
                    var distance = Vector3.Distance(transform.position, gameObject.transform.position);
                    if (closestTargetDistance > distance)
                    {
                        closestTargetDistance = distance;
                        closestTarget = gameObject;
                    }
                }
            }
        }

        return closestTarget;
    }
}
