﻿using UnityEngine;

/// <summary>
/// Class who contains all the crafting logic for the units.
/// </summary>
public class CraftingAbility : MonoBehaviour
{
    /// <summary>
    /// The different crafting related actions a unit can do.
    /// </summary>
    private enum Actions
    {
        None,
        Crafting,
        WoodGathering
    }

    #region Properties
    [Range(0.1f, 5f)]
    [Tooltip("The time needed to crat something")]
    public float timeToCraft = 1f;
    [Range(0.1f, 5f)]
    [Tooltip("The distance an agent should be to a tree at maximum to gather wood from it")]
    public float woodGatheringDistance = 3f;
    [Range(0f, 1f)]
    [Tooltip("The speed modifier of the agents when they carry wood")]
    public float woodCarryingSpeedMultiplier = 0.5f;
    public GameObject wallGameObject;
    public GameObject campfireGameObject;
    public WoodIconController woodIconController;

    private GameObject _area;
    private float _currentTime = 1f;
    private Structures _currentStructure;
    private Rigidbody _rb;
    private bool _isCarryingWood = false;
    private Actions _currentAction;
    #endregion

    private void Start()
    {
        _rb = GetComponent<Rigidbody>();
        _area = transform.parent.parent.gameObject;
    }

    /// <summary>
    /// Returns true if the unit is currently working (either gathering wood or building a structure).
    /// </summary>
    /// <returns>true if the unit is currently working</returns>
    public bool IsWorking()
    {
        return _currentTime < timeToCraft;
    }

    /// <summary>
    /// Returns true if the unit is currently carrying wood.
    /// </summary>
    /// <returns>true if the unit is currently carrying wood</returns>
    public bool IsCarryingWood()
    {
        return _isCarryingWood;
    }

    /// <summary>
    /// When this function is called the unit start building the given structure and can't move for some time.
    /// </summary>
    /// <param name="structureToBuild">The structure that will be built</param>
    /// <returns>If the crafting could start</returns>
    public bool StartCrafting(Structures structureToBuild)
    {
        if (IsWorking())
        {
            return false;
        }
        if (_isCarryingWood)
        {
            _currentStructure = structureToBuild;
            _currentTime = 0f;
            _currentAction = Actions.Crafting;
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// Returns a speed multiplier that change if the unit is curently carrying woods
    /// </summary>
    /// <returns>The speed multiplier</returns>
    public float SpeedMultiplier()
    {
        if (_isCarryingWood)
        {
            return woodCarryingSpeedMultiplier;
        }
        return 1f;
    }

    void Update()
    {
        if (IsWorking())
        {
            _rb.isKinematic = true;
            _rb.velocity = Vector3.zero;
            _currentTime += Time.deltaTime;

            if (!IsWorking())
            {
                _rb.isKinematic = false;
                switch (_currentAction)
                {
                    case Actions.Crafting:
                        BuildStructure();
                        break;
                    case Actions.WoodGathering:
                        CollectWood();
                        break;
                }
            }
        }
    }

    /// <summary>
    /// Called some time after the StartCrafting function, instantiate the structure built.
    /// </summary>
    private void BuildStructure()
    {
        _isCarryingWood = false;
        woodIconController.HideWoodIcon();
        Transform structureParent = transform.parent.parent;
        switch (_currentStructure)
        {
            case Structures.Wall:
                Instantiate(wallGameObject, new Vector3(transform.position.x, 0f, transform.position.z - 1f), Quaternion.identity, structureParent.Find("Walls"));
                break;
            case Structures.Campfire:
                Instantiate(campfireGameObject, new Vector3(transform.position.x, 0f, transform.position.z - 1f), Quaternion.identity, structureParent.Find("HeatSources"));
                break;
        }
    }

    /// <summary>
    /// Stops the unit for a time in order to gather wood.
    /// </summary>
    /// <returns>True if the unit was able to start gathering wood</returns>
    internal bool StartCollectingWood()
    {
        if (IsWorking() || _isCarryingWood || !CanGatherWood())
        {
            return false;
        }
        _currentTime = 0f;
        _currentAction = Actions.WoodGathering;
        return true;
    }

    /// <summary>
    /// Make the unit start carrying woods.
    /// </summary>
    private void CollectWood()
    {
        _isCarryingWood = true;
        woodIconController.ShowWoodIcon();
    }

    /// <summary>
    /// Returns the distance from the nearest tree of the area.
    /// </summary>
    /// <returns>The distance from the nearest tree</returns>
    private float GetNearestTreeDistance()
    {
        float nearestDistance = float.MaxValue;

        foreach (Transform tree in _area.FindComponentsInChildrenWithTag<Transform>("tree"))
        {
            float distance = Vector3.Distance(transform.position, tree.position);
            nearestDistance = (distance < nearestDistance) ? distance : nearestDistance;
        }

        return nearestDistance;
    }

    /// <summary>
    /// Returns true if the unit is close enough to a tree to gather wood.
    /// </summary>
    /// <returns></returns>
    public bool CanGatherWood()
    {
        return GetNearestTreeDistance() <= woodGatheringDistance;
    }

    /// <summary>
    /// Resets the wood carriyed by the unit.
    /// </summary>
    public void Reset()
    {
        _isCarryingWood = false;
        woodIconController.HideWoodIcon();
    }
}
