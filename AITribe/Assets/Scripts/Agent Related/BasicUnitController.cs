﻿using UnityEngine;

/// <summary>
/// A basic controller for any agent
/// </summary>
public class BasicUnitController : MonoBehaviour
{
    #region Properties
    [Header("Parameters")]
    [Tooltip("The maximum amount of HP of this agent")]
    [Range(1, 100)]
    public float MaxHP = 10.0f;
    [Tooltip("The duration needed for the agent to regain its default color after taking damage")]
    [Range(0f, 1f)]
    public float DamageAnimationTime = 1f;

    [Header("Colors")]
    [Tooltip("The default color of the agent")]
    public Color DefaultColor = Color.white;
    [Tooltip("The color of the agent when it takes damages")]
    public Color DamageColor = Color.red;
    [Tooltip("The color of the agent after his death")]
    public Color DeadColor = Color.gray;

    private MeshRenderer _meshRenderer;
    private float _damageAnimationTime = 0;


    public float CurrentHP { get; set; }

    #endregion

    #region Public methods

    /// <summary>
    /// Resets the unit after the end of an episode
    /// </summary>
    public virtual void Reset()
    {
        CurrentHP = MaxHP;
        _damageAnimationTime = 0;
        _meshRenderer.material.SetColor("_BaseColor", DefaultColor);
    }

    /// <summary>
    /// Deal damage of the given type to the agent
    /// </summary>
    /// <param name="amount">The amount of damage to deal</param>
    /// <param name="damageType">The type of damage dealt</param>
    public virtual void DealDamage(float amount, DamageType damageType)
    {
        if (IsDead())
        {
            return;
        }
        CurrentHP -= amount;
        if (IsDead())
        {
            ProcessDeath(damageType);
        }
        else
        {
            DamageColorChange();
        }
    }
    #endregion

    public virtual void Start()
    {
        _meshRenderer = GetComponent<MeshRenderer>();
        _meshRenderer.material.SetColor("_BaseColor", DefaultColor);

        CurrentHP = MaxHP;
    }

    void Update()
    {
        if (!IsDead())
        {
            AnimateDamage();
        }
    }

    #region Death and damage related enum and methods

    /// <summary>
    /// Returns whether or not the agent is dead
    /// </summary>
    /// <returns>true is the agent is dead, false otherwise</returns>
    public bool IsDead()
    {
        return CurrentHP <= 0;
    }

    /// <summary>
    /// Displays the death message of the agent and delete the agent
    /// </summary>
    /// <param name="causeOfDeath">What caused the death of the agent</param>
    protected virtual void ProcessDeath(DamageType causeOfDeath)
    {
        _meshRenderer.material.SetColor("_BaseColor", DeadColor);
        CurrentHP = 0;
        string deathText = gameObject.name + " ";
        switch (causeOfDeath)
        {
            case DamageType.Direct:
                deathText += "was killed by another agent.";
                break;
            case DamageType.Hunger:
                deathText += "starved to death.";
                break;
            case DamageType.Temperature:
                deathText += "died of extreme temperatures.";
                break;
        }
        Debug.Log(deathText);
    }

    /// <summary>
    /// Gradually change the color of the agent back to normal after damage;
    /// </summary>
    private void AnimateDamage()
    {
        if (_meshRenderer.material.color != DefaultColor)
        {
            _meshRenderer.material.SetColor("_BaseColor", Color.Lerp(DamageColor, DefaultColor, _damageAnimationTime));
            _damageAnimationTime += Time.deltaTime / DamageAnimationTime;
        }
    }

    /// <summary>
    /// Changes the color of the agent to visually indicate that it took damage
    /// </summary>
    protected void DamageColorChange()
    {
        _damageAnimationTime = 0;
        _meshRenderer.material.SetColor("_BaseColor", DamageColor);
    }
    #endregion

}
