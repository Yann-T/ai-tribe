﻿using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// A controller made specifically to control an hostile agent
/// </summary>
public class HostileAgentController : BasicUnitController
{
    private NavMeshAgent _agent;

    /// <summary>
    /// Set the destination of the agent to the given 3D point.
    /// </summary>
    /// <param name="destination">The destination</param>
    public void MoveToPosition(Vector3 destination)
    {
        _agent.SetDestination(destination);
    }

    /// <summary>
    /// Resets the controller
    /// </summary>
    public override void Reset()
    {
        base.Reset();
        _agent.SetDestination(transform.position);
    }

    public override void Start()
    {
        base.Start();
        _agent = GetComponent<NavMeshAgent>();
    }
}
