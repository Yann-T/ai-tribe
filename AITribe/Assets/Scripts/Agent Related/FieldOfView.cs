﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Represents the field of view (used only for the hostile agents)
/// </summary>
public class FieldOfView : MonoBehaviour
{
    #region Properties
    [Header("General")]
    [Tooltip("The radius of the sphere used to detect objects")]
    public float ViewDistance = 3f;
    #endregion

    #region Public methods
    /// <summary>
    /// Returns a list of the GameObjects the agent can see
    /// </summary>
    /// <returns>A list of GameObjects</returns>
    public List<GameObject> GetVisibleObjects()
    {
        List<GameObject> objectsInSight = new List<GameObject>();

        var hits = Physics.OverlapSphere(transform.position, ViewDistance);
        foreach (var hit in hits)
        {
            if (hit.gameObject == this.gameObject) continue;

            if (hit.transform.gameObject.layer != LayerMask.NameToLayer("Floor"))
            {
                //Debug.DrawLine(transform.position, hit.transform.position, Color.red);
                objectsInSight.Add(hit.gameObject);
            }
        }

        return objectsInSight;
    }
    #endregion

    void Update()
    {
        GetVisibleObjects();
    }

    /// <summary>
    /// Draws the sphere representing the field of view of the agent
    /// </summary>
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.grey;
        Gizmos.DrawWireSphere(transform.position, ViewDistance);
    }
}
