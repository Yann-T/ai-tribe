﻿/// <summary>
/// Represents the different ways an agent can die.
/// </summary>
public enum DamageType
{
    Direct,     // Indicates that an agent took direct damage from another one.
    Hunger,     // Indicates that an agent took damage from hunger.
    Temperature // Indicates that an agent took damage from a low or high body temperature.
}
