﻿using Unity.MLAgents;
using Unity.MLAgents.Sensors;
using UnityEngine;

/// <summary>
/// Class who contains all the agent-related logic specific to the scene
/// </summary>
public class TribeAgent : Agent
{
    private const float UNIT_DEATH_REWARD = -50f;           // The negative reward when an agent dies.
    private const float SUVIVAL_REWARD = 2f;                // The reward for each second of survival.
    private const float UNIT_HIT_REWARD = -10f;             // The negative reward when an agent get hit.
    private const float UNIT_MOVED_REWARD = -0.001f;        // A slight negative reward given when a unit moves.
    private const float UNIT_DID_INVALID_ACTION = -0.05f;   // The negative reward given when a unit tries to do an invalid action and fails.
    private const float UNIT_DID_GOOD_ACTION = 4.2f;        // A reward given when the unit successfully does a "complex" action.
    private const float ALLY_UNIT_SURVIVAL_REWARD = 1f;     // A reward given to a unit even after it's death if the other units in the same area survive (divided by total number of units)
    
    #region Properties
    public ResetComponent resetComponent;
    public TerrainUtils terrainUtils;

    private RayPerceptionSensorComponent3D _sensor;
    private UnitController _controller;
    private EnvironmentParameters _resetParams;

    public TribeSimulationSettings TribeSimulationSettings { get; set; }

    #endregion

    private void Awake()
    {
        TribeSimulationSettings = FindObjectOfType<TribeSimulationSettings>();
    }

    public override void Initialize()
    {
        _resetParams = Academy.Instance.EnvironmentParameters;

        _controller = GetComponent<UnitController>();
        _sensor = GetComponent<RayPerceptionSensorComponent3D>();
    }

    #region Rewards methods
    /// <summary>
    /// Must be called when a unit controlled by this agent take damage, add a negative reward to the agent.
    /// </summary>
    /// <param name="lifeLeft">The unit's life remaining</param>
    /// <param name="totalLife">The total life of the unit who got hit</param>
    public void UnitTookDamage(float lifeLeft, float totalLife)
    {
        AddReward(UNIT_HIT_REWARD);
    }

    /// <summary>
    /// Must be called when a unit die, add a negative reward to the agent.
    /// </summary>
    public void UnitDied()
    {
        AddReward(UNIT_DEATH_REWARD);
    }

    /// <summary>
    /// Must be called when a unit move.
    /// </summary>
    public void UnitMoved()
    {
        AddReward(UNIT_MOVED_REWARD);
    }

    /// <summary>
    /// Must be called when a unit tries to do an invalid action.
    /// </summary>
    public void UnitDidWrongAction()
    {
        AddReward(UNIT_DID_INVALID_ACTION);
    }

    /// <summary>
    /// Must be called when a unit successfully does a complex action (like gathering wood or building something).
    /// </summary>
    public void UnitDidGoodAction()
    {
        AddReward(UNIT_DID_GOOD_ACTION);
    }

    /// <summary>
    /// Called each time an action is received and adds a reward to the unit if the other units in teh same area are still alive
    /// </summary>
    private void AlliedUnitSuvival()
    {
        foreach(Transform ally in transform.parent)
        {
            if(ally.Equals(transform))
            {
                continue;
            }

            UnitController unitController = ally.gameObject.GetComponent<UnitController>();
            if(unitController != null && !unitController.IsDead()) {
                AddReward((ALLY_UNIT_SURVIVAL_REWARD * Time.deltaTime) / (transform.parent.childCount - 1));
            }
        }
    }
    #endregion

    #region Agent related methods
    public override void OnEpisodeBegin()
    {
        // TODO : Call this method only once per episode.
        resetComponent.ResetEpisode();
        gameObject.transform.position = terrainUtils.GetRandomSpawnPosition();
        _controller.Reset();

        ResetArea resetArea = transform.parent.parent.GetComponent<ResetArea>();
        if (resetArea != null)
        {
            resetArea.Reset();
        }
    }

    public override void OnActionReceived(float[] vectorAction)
    {
        if (!_controller.IsDead())
        {
            _controller.ProcessAction(vectorAction[0]);
            AddReward(SUVIVAL_REWARD * Time.deltaTime);
        }

        AlliedUnitSuvival();
    }

    public override void Heuristic(float[] actionsOut)
    {
        actionsOut[0] = 0;
        if (Input.GetKey(KeyCode.UpArrow))
        {
            actionsOut[0] = 1;
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            actionsOut[0] = 2;
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            actionsOut[0] = 3;
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            actionsOut[0] = 4;
        }
        if (_controller.HasCraftingAbility())
        {
            if (Input.GetKey(KeyCode.C))
            {
                actionsOut[0] = 5;
            }
            if (Input.GetKey(KeyCode.W))
            {
                actionsOut[0] = 6;
            }
            if (Input.GetKey(KeyCode.X))
            {
                actionsOut[0] = 7;
            }
        }
    }

    public override void CollectObservations(VectorSensor sensor)
    {
        sensor.AddObservation(_controller.CurrentHP);
        sensor.AddObservation(_controller.GetComponent<TemperatureDamage>().CurrentBodyTemperature);
        sensor.AddObservation(_controller.CraftingAbility.IsWorking());
        sensor.AddObservation(_controller.CraftingAbility.IsCarryingWood());
        sensor.AddObservation(_controller.CraftingAbility.CanGatherWood());
    }

    #endregion
}
