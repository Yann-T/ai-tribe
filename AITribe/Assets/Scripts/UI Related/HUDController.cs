﻿using UnityEngine;

/// <summary>
///  A simple class that controls the HUD
/// </summary>
public class HUDController : MonoBehaviour
{
    #region Properties
    [Header("Related components")]
    public TimeScaleController TimeScaleController;
    public RectTransform sidePanel;
    #endregion

    public void PauseButtonPressed()
    {
        TimeScaleController.Pause();
    }

    public void PlayButtonPressed()
    {
        TimeScaleController.Play();
    }

    public void FastForwardButtonPressed()
    {
        TimeScaleController.FastForward();
    }

    public void ShowSidePanel()
    {
        sidePanel.localScale = new Vector3(1, 1, 1);
    }

    public void HideSidePanel()
    {
        sidePanel.localScale = new Vector3(0, 0, 0);
    }
}
