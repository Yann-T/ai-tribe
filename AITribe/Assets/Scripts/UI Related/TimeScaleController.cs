﻿using UnityEngine;

/// <summary>
/// This component is used to change the speed at which the time flows in the scene
/// </summary>
public class TimeScaleController : MonoBehaviour
{
    [Header("Parameters")]
    [Range(1.5f, 100f)]
    public float FastForwardSpeed = 3f;

    /// <summary>
    /// Pauses the scene
    /// </summary>
    public void Pause()
    {
        Time.timeScale = 0;
    }

    /// <summary>
    /// Play the scene at a normal speed
    /// </summary>
    public void Play()
    {
        Time.timeScale = 1;
    }

    /// <summary>
    /// Play the scene a the fast forward speed
    /// </summary>
    public void FastForward()
    {
        Time.timeScale = FastForwardSpeed;
    }
}
