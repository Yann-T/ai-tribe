﻿using UnityEngine;

/// <summary>
/// This class allows the user to click on an agent on the screen to select it
/// </summary>
public class AgentSelection : MonoBehaviour
{
    #region Properties
    public HUDController HUDController;
    public AgentInfoDisplay agentInfoDisplay;
    #endregion

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastToSearchAnAgent();
        }
        else if (Input.GetKeyDown(KeyCode.Escape))
        {
            HUDController.HideSidePanel();
        }
    }

    /// <summary>
    /// Cast a ray to try and find the agent on which the user clicked
    /// </summary>
    private void RaycastToSearchAnAgent()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        // Test if the raycast hit something
        if (Physics.Raycast(ray, out hit, 100))
        {
            GameObject obj = hit.collider.gameObject;
            // Test if it was an agent
            if (obj.GetComponent<UnitController>() != null)
            {
                agentInfoDisplay.ChangeAgentDisplayed(obj);
                HUDController.ShowSidePanel();
            }
        }
    }
}
