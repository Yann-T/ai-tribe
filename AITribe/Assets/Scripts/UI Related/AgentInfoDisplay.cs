﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This class track some of the parameters of a selected agent and display them on the screen
/// </summary>
public class AgentInfoDisplay : MonoBehaviour
{
    #region Properties
    public Text agentName;
    public Slider healthSlider;
    public Slider temperatureSlider;
    public Image temperatureFill;
    public Image temperatureBackground;

    public Color DefaultTemperatureColor = Color.white;
    public Color HighTemperatureColor = new Color(255, 120, 120);
    public Color LowTemperatureColor = Color.blue;

    private GameObject _targetAgent;
    private UnitController _unitController;
    private TemperatureDamage _unitTemperature;
    private AreaTemperature _areaTemperature;
    private bool _unitHaveTemperatureComponent = false;
    #endregion

    void Update()
    {
        if (_targetAgent != null)
        {
            UpdateAgentInfo();
        }
    }
    /// <summary>
    /// Updates the various informations of the agent on the display.
    /// </summary>
    private void UpdateAgentInfo()
    {
        healthSlider.value = _unitController.CurrentHP;
        if (_unitHaveTemperatureComponent)
        {
            temperatureSlider.value = _unitTemperature.CurrentBodyTemperature;
            if (_unitTemperature.CurrentBodyTemperature < _areaTemperature.lowTemperatureDamageThreshold)
            {
                ChangeTemperatureBarToColdColor();
            }
            else if (_unitTemperature.CurrentBodyTemperature > _areaTemperature.highTemperatureDamageThreshold)
            {
                ChangeTemperatureBarToHotColor();
            }
            else
            {
                ChangeTemperatureBarToDefaultColor();
            }
        }
    }

    /// <summary>
    /// Changes the color of the temperature one to represent a cold temperature
    /// </summary>
    private void ChangeTemperatureBarToColdColor()
    {
        temperatureFill.color = LowTemperatureColor;
        temperatureBackground.color = Color.Lerp(LowTemperatureColor, Color.black, 0.5f);
    }

    /// <summary>
    /// Changes the color of the temperature one to represent a normal temperature
    /// </summary>
    private void ChangeTemperatureBarToDefaultColor()
    {
        temperatureFill.color = DefaultTemperatureColor;
        temperatureBackground.color = Color.Lerp(DefaultTemperatureColor, Color.black, 0.5f);
    }

    /// <summary>
    /// Changes the color of the temperature one to represent a hot temperature
    /// </summary>
    private void ChangeTemperatureBarToHotColor()
    {
        temperatureFill.color = HighTemperatureColor;
        temperatureBackground.color = Color.Lerp(HighTemperatureColor, Color.black, 0.5f);
    }

    /// <summary>
    /// Changes the agent from which the informations are displayed
    /// </summary>
    /// <param name="newAgent">The new target agent</param>
    public void ChangeAgentDisplayed(GameObject newAgent)
    {
        _targetAgent = newAgent;
        agentName.text = _targetAgent.name;
        _unitController = _targetAgent.GetComponent<UnitController>();
        healthSlider.maxValue = _unitController.MaxHP;
        _unitTemperature = _unitController.GetComponent<TemperatureDamage>();
        _unitHaveTemperatureComponent = _unitTemperature != null;

        if (_unitHaveTemperatureComponent)
        {
            _areaTemperature = _unitController.transform.parent.gameObject.GetComponentInParent<AreaTemperature>();
            if (_areaTemperature.ambientTemperature < 0)
            {
                temperatureSlider.minValue = _areaTemperature.ambientTemperature - 10;
            }
            else
            {
                temperatureSlider.minValue = 0;
            }
            temperatureSlider.maxValue = 60;
        }
        UpdateAgentInfo();
    }
}
