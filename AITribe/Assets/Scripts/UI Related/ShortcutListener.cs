﻿using UnityEngine;

/// <summary>
/// A simple class who listen to a specific shortcut and pause/play the game when it is pressed.
/// </summary>
public class ShortcutListener : MonoBehaviour
{
    private const KeyCode PAUSE_PLAY_TOGGLE_KEY = KeyCode.Space;

    public HUDController HUDController;

    void Update()
    {
        if (Input.GetKeyDown(PAUSE_PLAY_TOGGLE_KEY))
        {
            if (Time.timeScale > 0)
            {
                HUDController.PauseButtonPressed();
            }
            else
            {
                HUDController.PlayButtonPressed();
            }
        }
    }
}
