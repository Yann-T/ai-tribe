﻿using UnityEngine;

/// <summary>
/// Contains methods related to the area
/// </summary>
public class TerrainUtils : MonoBehaviour
{
    [Tooltip("The floor of the map")]
    public GameObject floor;
    [Tooltip("The radius of the map")]
    public float areaSize = 25;

    private Bounds _areaBounds;

    private void Awake()
    {
        _areaBounds = new Bounds(floor.transform.position, new Vector3(areaSize, 1, areaSize));
    }

    /// <summary>
    /// Returns a random point in the map
    /// </summary>
    /// <returns>A vector3 containing the position of the random point</returns>
    public Vector3 GetRandomSpawnPosition()
    {
        return NavMeshUtil.GetRandomPoint(_areaBounds.center, _areaBounds.size.x - 1) + Vector3.up;
    }
}
