﻿using UnityEngine;

/// <summary>
/// Contains global settings of the simulation
/// </summary>
public class TribeSimulationSettings : MonoBehaviour
{
    [Range(1f, 20f)]
    public float agentSpeed;
}
