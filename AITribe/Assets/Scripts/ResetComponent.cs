﻿using UnityEngine;

/// <summary>
/// This class contains functions used to reset an area between two episodes
/// </summary>
public class ResetComponent : MonoBehaviour
{
    public TerrainUtils terrainUtils;

    private HostileAgentSimpleAI[] _hostile;

    private void Start()
    {
        _hostile = GetComponentsInChildren<HostileAgentSimpleAI>();
    }

    /// <summary>
    /// Resets the area for the next episode
    /// </summary>
    public void ResetEpisode()
    {
        ResetHostilePosition();
    }

    /// <summary>
    /// Resets the positions of the hostile agents
    /// </summary>
    private void ResetHostilePosition()
    {
        if (_hostile != null)
        {
            foreach (var hostile in _hostile)
            {
                hostile.ChangePosition(terrainUtils.GetRandomSpawnPosition());
            }
        }
    }
}
