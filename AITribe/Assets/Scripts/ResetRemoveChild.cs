﻿using System.Collections.Generic;
using UnityEngine;

public class ResetRemoveChild : MonoBehaviour
{
    public List<GameObject> objectsToKeepBetweenReset;

    public void Reset()
    {
        foreach (Transform tra in transform)
        {
            if (!objectsToKeepBetweenReset.Contains(tra.gameObject))
            {
                Destroy(tra.gameObject);
            }
        }
    }
}
