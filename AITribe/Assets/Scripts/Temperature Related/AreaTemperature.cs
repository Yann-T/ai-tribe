﻿using UnityEngine;

/// <summary>
/// Handles the temperature related logic of an area
/// </summary>
public class AreaTemperature : MonoBehaviour
{
    #region Properties
    [Header("Temperatures")]
    [Range(-60f, 80f)]
    public float ambientTemperature = 20f;

    [Header("Temperature damage settings")]
    [Tooltip("Time (in seconds) between two damages caused by extreme temperature to an agent")]
    public float timeBetweenTemperatureDamage = 2f;[Range(-30, 60f)]
    [Tooltip("If the agent's temperature is less than this value it take damage")]
    public float lowTemperatureDamageThreshold = -10f;
    [Range(-30, 60f)]
    [Tooltip("If the agent's temperature is more than this value it take damage")]
    public float highTemperatureDamageThreshold = 40f;
    #endregion

    private HeatEmitter[] _heatEmitters;

    /// <summary>
    /// Recovers the list of all the heat emitters child of the gameobject
    /// </summary>
    private void CheckNewHeatEmitter()
    {
        _heatEmitters = GetComponentsInChildren<HeatEmitter>();
    }

    /// <summary>
    /// Returns the temperature at the given position
    /// </summary>
    /// <param name="position">The position at which we want the temperature</param>
    /// <returns>The temperature at the position</returns>
    public float GetTemperatureAtPosition(Vector3 position)
    {
        CheckNewHeatEmitter();
        float temperatureAtPoint = ambientTemperature;
        foreach (var emitter in _heatEmitters)
        {
            var distanceFromEmitter = Vector3.Distance(position, emitter.transform.position);
            if (distanceFromEmitter <= emitter.radius)
            {
                temperatureAtPoint = MeanOfTwoValues(temperatureAtPoint, emitter.GetStrenghtAtDistance(distanceFromEmitter));
            }
        }
        return temperatureAtPoint;
    }

    /// <summary>
    /// Returns the mean value between two values
    /// </summary>
    /// <param name="val1">First value</param>
    /// <param name="val2">Second value</param>
    /// <returns>The mean of the two values</returns>
    private float MeanOfTwoValues(float val1, float val2)
    {
        return (val1 + val2) / 2;
    }
}
