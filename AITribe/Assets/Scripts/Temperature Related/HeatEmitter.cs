﻿using System;
using UnityEngine;

/// <summary>
/// Represents a heat source on the scene
/// </summary>
public class HeatEmitter : MonoBehaviour
{
    #region Properties
    [Range(0f, 20f)]
    [Tooltip("The radius of the range of the heat source")]
    public float radius = 5f;
    [Tooltip("The temperature at the center of the range")]
    public float temperature = 100f;
    #endregion

    /// <summary>
    /// Returns the strenght of the emitter at the given distance
    /// </summary>
    /// <param name="distance">The distance from the center of this emitter</param>
    /// <returns>A value between 0 and 1 with 0 being no heat and 1 being the full power of the emitter</returns>
    public float GetStrenghtAtDistance(float distance)
    {
        if (distance > radius)
        {
            return 0;
        }
        return temperature - (float)Math.Exp((distance / radius) * 3.0 - 3.0) * temperature;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}
