﻿using UnityEngine;

/// <summary>
/// Controls the visibility of the icon indicating if the agent is currently carrying wood.
/// </summary>
public class WoodIconController : MonoBehaviour
{
    private GameObject _icon;

    void Start()
    {
        _icon = transform.GetChild(0).gameObject;
    }

    /// <summary>
    /// Shows the icon on top of the agent's head
    /// </summary>
    public void ShowWoodIcon()
    {
        _icon.SetActive(true);
    }

    /// <summary>
    /// Hides the icon
    /// </summary>
    public void HideWoodIcon()
    {
        _icon.SetActive(false);
    }

    
}
